# atl-dehydrated

Simple package wrapper for dehydrated.

See https://github.com/lukas2511/dehydrated



## Install

```shell
npm install git+https://bitbucket.org/ATLTed/atl-dehydrated.git#master
```
